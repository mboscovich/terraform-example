# Máquina virtual con Terraform

Este repositorio contiene las definiciones necesarias para la creación
de un entorno local de pruebas de [`Terraform`](https://www.terraform.io/) versión 0.12, siguiendo la [documentación oficial](https://learn.hashicorp.com/terraform/getting-started/install.html). 
Estas permiten crear una maquina virtual, por medio de [Vagrant](https://www.vagrantup.com/), en [Virtualbox](https://www.virtualbox.org/) y aprovisionada mediante [Ansible](https://www.ansible.com). 

## Modo de uso

### Prerequisitos

Se requiere tener instalado
[Ansible](https://www.ansible.com), [Vagrant](https://www.vagrantup.com), [Virtualbox](https://www.virtualbox.org/) y [Git](https://www.git-scm.com).

Los siguientes comandos le permiten instalar este software necesario en un entorno Debian GNU/Linux estable:

```shell
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | sudo tee /etc/apt/sources.list.d/ansible.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update
sudo apt install vagrant ansible virtualbox git
```
NOTA: En caso de problemas al iniciar la máquinas, se recomienda utilizar el paquete provisto por vagrant en https://www.vagrantup.com/downloads.html.

### Descargar archivos de definición

Empiece por clonar este repositorio
```bash
git clone https://gitlab.com/mboscovich/terraform-example.git
```

Este comando creará una carpeta `terraform-example`. Dentro de ella, se encuentra un archivo llamado `Vagrantfile` que contiene la definición de la VM que utilizaremos, creándola en Virtualbox.

### Credenciales de AWS
El ejemplo utiliza Amazon AWS para las pruebas, y requiere que en el archivo ~/.aws/credentials estén las credenciales de aws para que terraform se conecte y lo administre.
Si no cuenta con estas credenciales, comente la siguiente línea dentro del archivo Vagrantfile
```
#  config.vm.synced_folder "~/.aws", "/home/vagrant/.aws"
```
### Iniciar la VM

Luego se debe acceder al directorio y ejecutar vagrant up
```bash
cd terraform-example
vagrant up
```
Esto iniciara el proceso de creación, aprovisionamiento y configuración de la VM en su máquina local, la que estará lista para usar.

Este proceso la primera vez demorará unos minutos porque debe
descargar un template del sistema operativo Debian 9 desde internet,
pero en sucesivas ejecuciones este proceso será mucho más ágil.

### Ingresar a la VM

Para ingresar a la VM, ejecute vagran ssh simplemente, parado dentro del directorio donde esta el 
Vagrantfile

```bash
cd terraform-example
vagrant ssh
```

Luego ya tendrá disponible terraform para trabajar y hacer pruebas

```bash
vagrant@contrib-stretch:~$ terraform --version
Terraform v0.12.2

```

### Detener la VM

Para detener la ejecución de la VM, ejecute
```bash
vagrant halt
```

### Eliminar la VM

Si desea borrar la máquina virtual, ejecute

```bash
vagrant destroy
```

Esto eliminará por completo la VM, y la próxima vez que ejecute **vagrant up** se crearán nuevamente  definida como el primer día.