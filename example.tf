terraform {
  backend "remote" {
    organization = "mboscovich"

    workspaces {
      name = "Dev-QA"
    }
  }
}